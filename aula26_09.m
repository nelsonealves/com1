%% Aula 17 - Cosseno levantado
% 24/09

clear all;
clc;
close all;

%% Parametros iniciais
sps = 80;     % Amostras por simbolo (fator de superamostragem)
SNR = 10;     % snr do filtro rx
Nfft = 2^15;
Rb = 1e3;      % Taxa te bits [bit/s]
Tb = 1 / Rb;   % Tempo de bit [s]
Ta = Tb / sps; % Intervalo de amostragem [s]
fa = 1 /Ta;    % Frequencia de amostragem [amostrar/Hz]
Nb = 10000;
bits = randi([0 1], 1, Nb);
N_bits = length(bits);


%% Formatação
x = 2 * bits - 1;
x_up = upsample(x,sps);

beta = 0.4; %fator de hollof (a mais que o pulso)
span = 31; % quantidade de voltas (numero impar)

%% Transmissão 
%%h_TX = srrc(span, 0.4, sps); USAR QUANDO MATLAB FOR 2012 BRO
h_TX = rcosdesign(beta,span,sps, 'sqrt');
%h_TX = h_TX / max(h_TX);
h_RX = fliplr(h_TX);
atraso_TX = (length(h_TX)-1)/2;
atraso_RX = (length(h_RX)-1)/2;
f = ((-Nfft/2:(Nfft/2)-1)./Nfft).*fa;
sinal_formatado = conv(h_TX, x_up); % ou convoluçao
sinal_formatado_f = fftshift(fft(sinal_formatado,Nfft));
%% Recepção
sinal_recebido = sinal_formatado;
sinal_recebido = awgn(sinal_recebido, SNR, 'measured');
z = filter(h_RX, 1, sinal_recebido);
z_f = fftshift(fft(z,Nfft));
N_samp = length(z);
t = (0 : N_samp-1)* Ta;


% Multiplica o comprimento do filtro pela duracao das amostras
t0 = Ta *(length(h_TX) - 1);
tf = t0 + (N_bits-1) * Tb;

t_bits = t0 : Tb : tf;

figure(1)
subplot(211);
plot(t/1e-3, sinal_formatado);
hold on;
stem(t_bits/1e-3, x);
xlabel('t(ms)');
subplot(212)
plot(t/1e-3, z);
hold on;
stem(t_bits/1e-3, x);

figure(2)
subplot(211)
plot(f, 20*log10(abs(sinal_formatado_f)));
subplot(212)
plot(f,20*log10(abs(z_f)));