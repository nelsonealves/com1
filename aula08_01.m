% Aula 1 - Exercicio I

clear all;
close all;
clc;

f = 1000;
fa = 10*f;               % freq. amostragem               
t = [0:1/fa:200e-3];     % tempo
y = cos(2*pi*f*t);

figure(1)
plot(t,y);


Y = fft(y);
figure(2)
f1 = [0:5:fa];
plot(f1,abs(Y));


Y = fftshift(Y);
f2 = [-fa/2:5:fa/2];
figure(3);
plot(f2,abs(Y));

