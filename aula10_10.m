%% Aula 23
% 10/10

clear all;
close all;
clc;

M = 16;         % ordem modulacao
k = log2(M);    % numero de bits por simbolo
Nbits = 20000;  % numero de bits transmitidos
bits = randi([0 1], 1, Nbits);  
bits_reshaped = reshape(bits, k, [])';
symbols = bi2de(bits_reshaped, 'left-msb');
s = qammod(symbols, M);     

r = awgn(s, 20, 'measured');
symbols_hat = qamdemod(r, M);

% % % % % aula do dia 10 de outubro
bis_hat_reshape = de2bi(symbols_hat, k, 'left-msb');
bits_hat = reshape(bis_hat_reshape', 1, Nbits);

BER = sum(bits ~= bits_hat);

figure
grid on; hold on;
scatter(real(r), imag(r), 'r.');
scatter(real(s),imag(s), 'bo');

%%
% Exercicio matlab
% Utilizar as funçoes de modulacoes digitais do matlab para simular uma
% transmissao utilizano as modulaçoes 16-QAM e 16-FSK



