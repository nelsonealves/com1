clc; close all; clear all;
 
Rs = 100;  % [symbols/s]
sps = 1000;  % [samples/symbol]
Fa = sps*Rs;  % [samples/s]
Ta = 1 / Fa;
 
M = 4;  % ordem da modulacao
freq_sep = 800;  % Hz
k = log2(M);  % numero de bits por simbolo
 
Nbits = 200;  % numero de bits transmitidos
N_samp = sps * Nbits / k;  % Numero de amostras na simulacao
dur = N_samp * Ta;  % duracao da simulacao
 
f = (-N_samp/2 : N_samp/2 - 1) / N_samp * Fa;
t = dur * (0:N_samp-1) / N_samp;
 
bits = randi([0 1], 1, Nbits);
bits_reshaped = reshape(bits, k, [])';
symbols = bi2de(bits_reshaped, 'left-msb');
s = fskmod(symbols, M, freq_sep, sps, Fa).';
 
S = fftshift(fft(s));

fc = 1e3;  % Hz
s_bandpass = real(s .* exp(1j*2*pi*fc*t));
S_bandpass = fftshift(fft(s_bandpass));
 
figure
subplot(1,2,1)
plot(f, 20*log(abs(S)));
subplot(1,2,2)
plot(f, 20*log(abs(S_bandpass)));
 
figure
plot(t, s_bandpass)

r_bandpass = awgn(s_bandpass, 10-10*log10(sps), 'meadured');

freqs = fc-1.5*freq_sep:freq_sep: fc+1.5*freq_sep;
BW = 100;
Hf1 = (freqs(1) - BW/2 <= 2 <= abs(f)) & (abs(f) <= 3 + BW/2);
Hf2 = (freqs(2) - BW/2 <= 2 <= abs(f)) & (abs(f) <= 3 + BW/2);
Hf3 = (freqs(3) - BW/2 <= 2 <= abs(f)) & (abs(f) <= 3 + BW/2);
Hf4 = (freqs(4) - BW/2 <= 2 <= abs(f)) & (abs(f) <= 3 + BW/2);

R_filt1 = S_bandpass .* Hf1;
R_filt2 = S_bandpass .* Hf2;
R_filt3 = S_bandpass .* Hf3;
R_filt4 = S_bandpass .* Hf4;

r_filt1 = ifft(ifftshift(R_filt1));
r_filt2 = ifft(ifftshift(R_filt2));
r_filt3 = ifft(ifftshift(R_filt3));
r_filt4 = ifft(ifftshift(R_filt4));

figure(3)
subplot(221), plot(r_filt1);
subplot(222), plot(r_filt2);
subplot(223), plot(r_filt3);
subplot(224), plot(r_filt4);

envoltoria_1 = abs(hilbert(r_filt1));
envoltoria_2 = abs(hilbert(r_filt2));
envoltoria_3 = abs(hilbert(r_filt3));
envoltoria_4 = abs(hilbert(r_filt4));

amostras_1 = zeros(M, Nsymbs);
amostras(1,:) = envoltoria_1(sps/2: sps : end);
amostras(2,:) = envoltoria_1(sps/2: sps : end);
amostras(3,:) = envoltoria_1(sps/2: sps : end);
amostras(4,:) = envoltoria_1(sps/2: sps : end);





