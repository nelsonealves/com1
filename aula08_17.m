clc; clear all;close all;

m1 = 0.5;
m2 = 0.75;
m3 = 1;
m4 = 0.32701;

Ac = 1;

fm = 1000;
fc = 10000;
fa = 200*fc;

t = [0:1/fa:5e-3];

mt = cos(2*pi*fm.*t);
ct = Ac*cos(2*pi*fc.*t);
st1 = Ac*(1+m1*cos(2*pi*fm.*t)).*cos(2*pi*fc.*t);
st2 = Ac*(1+m2*cos(2*pi*fm.*t)).*cos(2*pi*fc.*t);
st3 = Ac*(1+m3*cos(2*pi*fm.*t)).*cos(2*pi*fc.*t);
st4 = Ac*(1+m4*cos(2*pi*fm.*t)).*cos(2*pi*fc.*t);

figure(1)
subplot(411)
plot(t,st1)
subplot(412)
plot(t,st2)
subplot(413)
plot(t,st3)
subplot(414)
plot(t,st4)

f = -fc/2:fc/2;

f1 = fftshift(fft(st1));
f2 = fftshift(fft(st2));
f3 = fftshift(fft(st3));
f4 = fftshift(fft(st4));

figure(2)
subplot(411)
plot(f,abs(f1))
subplot(412)
plot(f,abs(f2))
subplot(413)
plot(f,abs(f3))
subplot(414)
plot(f,abs(f4))

%% Detector de envoltória

st1_abs = abs(st1);
