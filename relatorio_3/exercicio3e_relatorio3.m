clear all; close all; clc;
A1 = 1;
sps = 10; %total de bits para super amostrar
span = 1;
beta = 0.5;
Rb = 100e3;
limiar = A1/2;
info = randi([0 1],1, Rb);
info_up = upsample(info, sps); %super amostrar
filtro_raiz = rcosdesign(beta,span,sps, 'sqrt');%filtro raiz cosseno levantado
sinal_formatado = filter(filtro_raiz,1,info_up);%passando pelo filtro formatador 
filtro_rx = fliplr(filtro_raiz);

for SNR = 0:20
sinal_rx = awgn(sinal_formatado, SNR);

% filtrando
sinal_T_filtrado = filter(filtro_rx,1,sinal_rx);%passando pelo filtro casado
sinal_T = sinal_T_filtrado(sps:sps:end); %amostragem de sinal
info_hat = sinal_T > limiar; %deteccao de simbolo
[err1(SNR+1),taxa1(SNR+1)] = biterr(info,info_hat);
end

figure,
semilogy([1:SNR], taxa1(1:SNR)); hold on;
xlabel('SNR')
ylabel('BER')
title('Filtro raiz cosseno levantado ')
