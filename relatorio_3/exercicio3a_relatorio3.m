clear all; close all; clc;
A1 = 1;
A2 = 2;
Nsamp = 10; %total de bits para super amostrar
Rb = 10e3;
limiar1_NRZ = A1/2;
limiar2_NRZ = A2/2;
info = randi([0 1],1, Rb);
info1_NRZ = info*A1;%amplitude 1v
info2_NRZ = info*A2;%amplitude 2v

info1_up = upsample(info1_NRZ, Nsamp); %super amostrar
info2_up = upsample(info2_NRZ, Nsamp); %super amostrar
filtro_NRZ = ones(1, Nsamp); %cria um filtro ideal
sinal1_NRZ = filter(filtro_NRZ,1,info1_up);%passando pelo filtro formatador 
sinal2_NRZ = filter(filtro_NRZ,1,info2_up);%passando pelo filtro formatador

for SNR = 0:50
sinal1_NRZ_rx = awgn(sinal1_NRZ, SNR, 'measured');
sinal2_NRZ_rx = awgn(sinal2_NRZ, SNR, 'measured');

% Sem filtrar 1
sinal1_T = sinal1_NRZ_rx(Nsamp/2:Nsamp:end); %amostragem de sinal
info1_hat_NRZ_rx = sinal1_T > limiar1_NRZ; %deteccao de simbolo
[err1(SNR+1),taxa1(SNR+1)] = biterr(info,info1_hat_NRZ_rx);

% Sem filtrar 2
sinal2_T = sinal2_NRZ_rx(Nsamp/2:Nsamp:end); %amostragem de sinal
info2_hat_NRZ_rx = sinal2_T > limiar2_NRZ; %deteccao de simbolo
[err2(SNR+1),taxa2(SNR+1)] = biterr(info,info2_hat_NRZ_rx);
end

figure,
semilogy([1:SNR], taxa1(1:SNR)); hold on;
semilogy([1:SNR], taxa2(1:SNR));
xlabel('SNR')
ylabel('BER')
title('NRZ Unipolar 1V e 2V sem filtro')
