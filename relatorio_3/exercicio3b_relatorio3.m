clc; clear all; close all;

A = 1;
Nsamp = 100; %total de bits para super amostrar
Rb = 10e3;
limiar_NRZ = A/2;

info = randi([0 1],1,Rb);
info = info*A;

info_up = upsample(info, Nsamp); %super amostrar
filtro_NRZ = ones(1, Nsamp); %cria um filtro ideal

sinal_NRZ = filter(filtro_NRZ,1,info_up);%passando pelo canal

for SNR = 0:50
sinal_NRZ_rx = awgn(sinal_NRZ, SNR, 'measured');

% Sem filtrar
sinal_T = sinal_NRZ_rx(Nsamp/2:Nsamp:end); %amostragem de sinal
info_hat_NRZ_rx = sinal_T > limiar_NRZ; %deteccao de simbolo
[err1(SNR+1),taxa1(SNR+1)] = biterr(info,info_hat_NRZ_rx);

% Filtrar
sinal_NRZ_rx_filtrado = filter(filtro_NRZ,1,sinal_NRZ_rx);
sinal_T_filtrado = sinal_NRZ_rx_filtrado(Nsamp:Nsamp:end); %amostragem de sinal
info_hat_NRZ_rx_filtrado = sinal_T_filtrado > limiar_NRZ; %deteccao de simbolo
[err2(SNR+1),taxa2(SNR+1)] = biterr(info,info_hat_NRZ_rx_filtrado);
end

figure,
semilogy([1:SNR], taxa1(1:SNR)); hold on;
semilogy([1:SNR], taxa2(1:SNR));
xlabel('SNR')
ylabel('BER')
title('NRZ Unipolar 1V com e sem filtro casado')
