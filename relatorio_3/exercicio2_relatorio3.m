clc; clear all; close all;

A = 1;
Nsamp = 100; %total de bits para super amostrar
Rb = 10e3;
limiar_NRZ = A/2;
SNR = 10;

info = [1 0 0 1 1 0 1 1 0 1] 

t = [0:1/(Rb*Nsamp):(length(info)/Rb)-(1/(Rb*Nsamp))];

info_up = upsample(info, Nsamp); %super amostrar
filtro_NRZ = ones(1, Nsamp); %cria um filtro ideal

info_NRZ = filter(filtro_NRZ,1,info_up);
sinal_NRZ = info_NRZ*A;

sinal_NRZ_rx = awgn(sinal_NRZ, SNR, 'measured');

% Sem filtrar
sinal_T = sinal_NRZ_rx(Nsamp/2:Nsamp:end); %amostragem de sinal
info_hat_NRZ_rx = sinal_T > limiar_NRZ %deteccao de simbolo
num_erro_sinal_rx = sum(xor(info,info_hat_NRZ_rx));
taxa_erro_NRZ_rx = num_erro_sinal_rx/length(info);

% Filtrar
filtro = fir1(50,(1/Rb)*Nsamp);
sinal_NRZ_rx_filtrado = filter(filtro,1,sinal_NRZ_rx);

sinal_T_filtrado = sinal_NRZ_rx_filtrado(Nsamp:Nsamp:end); %amostragem de sinal
info_hat_NRZ_rx_filtrado = sinal_T_filtrado > limiar_NRZ %deteccao de simbolo
num_erro_sinal_rx_filtrado = sum(xor(info,info_hat_NRZ_rx_filtrado));
taxa_erro_NRZ_rx_filtrado = num_erro_sinal_rx_filtrado/length(info);

figure(1)
plot(t,sinal_NRZ)
xlabel('Informação transmitida')
xlim([0 (1/Rb)*10]);
ylim([-0.5 1.5])

figure(2)
plot(t,sinal_NRZ_rx)
xlabel('Sinal Sem filtro Casado')
xlim([0 (1/Rb)*10]);

figure(3)
plot(t,sinal_NRZ_rx_filtrado)
xlabel('Sinal Com filtro Casado')
xlim([0 (1/Rb)*10]);
