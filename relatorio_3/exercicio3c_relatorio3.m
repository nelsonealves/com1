clear all; close all; clc;
A = 1;
Nsamp = 1; %total de bits para super amostrar
Rb = 100e3;
limiar1_NRZ = A/2;
limiar2_NRZ = 0;
info = randi([0 1],1, Rb);
infou_NRZ = info*A;%amplitude 2v
infob_NRZ = info*(2*A)-A;%amplitude 1v a -1v

infou_up = upsample(infou_NRZ, Nsamp); %super amostrar
infob_up = upsample(infob_NRZ, Nsamp); %super amostrar

filtro_NRZ = ones(1, Nsamp); %cria um filtro ideal
sinalu_NRZ = filter(filtro_NRZ,1,infou_up);%passando pelo filtro formatador 
sinalb_NRZ = filter(filtro_NRZ,1,infob_up);%passando pelo filtro formatador

for SNR = 0:20
    %passano pelo canal awgn
    sinalu_NRZ_rx = awgn(sinalu_NRZ, SNR, 'measured');
    sinalb_NRZ_rx = awgn(sinalb_NRZ, SNR, 'measured');
    
    % Filtrar uipolar
    sinalu_NRZ_rx_filtrado = filter(filtro_NRZ,1,sinalu_NRZ_rx)/Nsamp;
    sinalu_T_filtrado = sinalu_NRZ_rx_filtrado(Nsamp:Nsamp:end); %amostragem de sinal
    infou_hat_NRZ_rx_filtrado = sinalu_T_filtrado > limiar1_NRZ; %deteccao de simbolo
    [err1(SNR+1),taxa1(SNR+1)] = biterr(info,infou_hat_NRZ_rx_filtrado);
    
    % Filtrar bipolar
    sinalb_NRZ_rx_filtrado = filter(filtro_NRZ,1,sinalb_NRZ_rx)/Nsamp;
    sinalb_T_filtrado = sinalb_NRZ_rx_filtrado(Nsamp:Nsamp:end); %amostragem de sinal
    infob_hat_NRZ_rx_filtrado = sinalb_T_filtrado > limiar2_NRZ; %deteccao de simbolo
    [err2(SNR+1),taxa2(SNR+1)] = biterr(info,infob_hat_NRZ_rx_filtrado);
end

figure,
semilogy([1:SNR], taxa1(1:SNR)); hold on;
semilogy([1:SNR], taxa2(1:SNR));
xlabel('SNR')
ylabel('BER')
title('NRZ Unipolar e Bipolar com filtro casado')
legend('Unipolar','Bipolar')
