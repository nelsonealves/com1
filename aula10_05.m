close all;
clear all;
clc;

M = 16;
k = log2(M);
Nbits = 200;
bits = randi([0 1], 1, Nbits);
bits_transpose = reshape(bits, 4, [])';
symbols = bi2de(bits_transpose, 'left-msb');
s = qammod(symbols, M);

r = awgn(s , 20, 'measured');

symbols_hat = qamdemod(r, M);

figure
scatter(real(s), imag(s), 'r');
grid on;
hold on;
scatter(real(r), imag(r), 'b');