close all;
clear all;
clc;
M = 16;
k = log2(M);
Ns = 100000;
Nb = Ns * k;
EsNo_dB_list = -2:20;
bits = randi([0 1], 1, Nb);
symbols = bi2de(reshape(bits, k, Ns)', 'left-msb');
s = qammod(symbols, M);
Es = mean(abs(s).^2);
    
ii = 1;
SER = zeros(size(EsNo_dB_list));
Ps = zeros(size(EsNo_dB_list));
for EsNo_dB = EsNo_dB_list

    EsNo = 10^(EsNo_dB / 10);
    No = Es / EsNo;
    Re_w = randn(size(s)) * sqrt(No/2);
    Im_w = randn(size(s)) * sqrt(No/2);
    w = Re_w +1j * Im_w;
    r = s + w;
    symbols_hat = qamdemod(r, M);
    SER(ii) = mean(symbols ~= symbols_hat);
    Ps(ii) = 3 * qfunc(sqrt(EsNo/5));
    ii = ii + 1;
    
end

semilogy(EsNo_dB_list, SER, 'r-o');
grid on;
hold on;
semilogy(EsNo_dB_list, Ps, 'b.')