clear all;
close all;
clc;

SNR = 10;
Nsamp = 100;

filtro_tx = ones(1, Nsamp);
filtro_rx = flip(filtro_tx);
info = [0 1 1 0 0 1];

info_up = upsample(info, Nsamp);
sinal_tx = filter(filtro_tx, 1, info_up);

sinal_rx = awgn(sinal_tx, SNR, 'measured');
sinal_casado = filter(filtro_rx, 1, sinal_rx)/Nsamp;

figure(1)
subplot(311)
plot(sinal_tx);
subplot(312)
plot(sinal_rx);
subplot(313)
plot(sinal_casado);




