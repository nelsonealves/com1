clear all;
close all;

span = 17;
sps = 100;
N = 1000;
%%info = randint(1, N);
info = [1 0 1 1];
sinal = (info*2) -1;

Rb = 1e3;
Tb = 1/ Rb;
Ta = Tb/ sps;
fa = 1/Ta;

sinal_sample = upsample(sinal, 5);

h_cos_t = rcosdesign(1, span, sps);
h_cos_f = abs(fftshift(fft(h_cos_t)));
figure(1)
subplot(211)
plot(h_cos_t);
subplot(212)
plot(h_cos_f);

sinal_formatado = conv(h_cos_t, sinal_sample);

figure(2)
subplot(311)
plot(sinal_formatado);
%xlim([0 300]);
subplot(312)
stem(sinal_sample);
%xlim([0 100]);
subplot(313)
plot(abs(fftshift(fft(sinal_formatado))));
