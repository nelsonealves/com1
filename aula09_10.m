clear all;
close all;
clc;

%% Parâmetros iniciais
A = 5;
Nsamp = 100;
Rb = 10e3;
Eb = 15; %Energia de bit
Eb_No_1 = 5;
Eb_No_2 = 3;
Eb_No_3 = 1;
Eb_No_4 = 0;

%% Ruido
var_ruido1 = Eb/(10^((Eb_No_1)/(10)*2));
var_ruido2 = Eb/(10^((Eb_No_2)/(10)*2));
var_ruido3 = Eb/(10^((Eb_No_3)/(10)*2));
var_ruido4 = Eb/(10^((Eb_No_4)/(10)*2));

limiar_NRZ = 7;
limiar_RZ = A/2;

%info = [1 0 0 1 1 0 1 1 0 1];
info = randint(1, 10e3);
t = 0:(1/(Rb*Nsamp)): (length(info)/Rb) - (1/(Rb*Nsamp));
info_up = upsample(info, Nsamp);
filtro_NRZ = ones(1, Nsamp);
info_NRZ = filter(filtro_NRZ, 1, info_up);
sinal_NRZ = info_NRZ*(2*A)-A;
sinal_RZ = info_NRZ*A;

%% Codificação sem ruido
figure(1)
subplot(211)
plot(t, sinal_NRZ);
ylim([-6 6]);
xlim([0 (1/Rb)*10]);

subplot(212)
plot(t, sinal_RZ);
ylim([-6 6]);
xlim([0 (1/Rb)*10]);

sinal_NRZ_rx1 = sinal_NRZ + sqrt(var_ruido1)*randn(1, length(sinal_NRZ));
sinal_NRZ_rx2 = sinal_NRZ + sqrt(var_ruido2)*randn(1, length(sinal_NRZ));
sinal_NRZ_rx3 = sinal_NRZ + sqrt(var_ruido3)*randn(1, length(sinal_NRZ));
sinal_NRZ_rx4 = sinal_NRZ + sqrt(var_ruido4)*randn(1, length(sinal_NRZ));

sinal_RZ_rx1 = sinal_RZ + sqrt(var_ruido1)*randn(1, length(sinal_RZ));
sinal_RZ_rx2 = sinal_RZ + sqrt(var_ruido2)*randn(1, length(sinal_RZ));
sinal_RZ_rx3 = sinal_RZ + sqrt(var_ruido3)*randn(1, length(sinal_RZ));
sinal_RZ_rx4 = sinal_RZ + sqrt(var_ruido4)*randn(1, length(sinal_RZ));

%% Codificação NRZ com ruido

figure (2)
subplot(421)
plot(t, sinal_RZ_rx1)
ylim([-10 10]);
xlim([0 (1/Rb)*10]);

subplot(423)
plot(t, sinal_NRZ_rx2)
ylim([-10 10]);
xlim([0 (1/Rb)*10]);

subplot(425)
plot(t, sinal_RZ_rx3)
ylim([-10 10]);
xlim([0 (1/Rb)*10]);

subplot(427)
plot(t, sinal_NRZ_rx4)
ylim([-10 10]);
xlim([0 (1/Rb)*10]);

%% Codificação RZ com ruido
subplot(422)
plot(t, sinal_RZ_rx1)
ylim([-10 10]);
xlim([0 (1/Rb)*10]);

subplot(424)
plot(t, sinal_NRZ_rx2)
ylim([-10 10]);
xlim([0 (1/Rb)*10]);

subplot(426)
plot(t, sinal_RZ_rx3)
ylim([-10 10]);
xlim([0 (1/Rb)*10]);

subplot(428)
plot(t, sinal_RZ_rx3)
ylim([-10 10]);
xlim([0 (1/Rb)*10]);

%% Filtro pra diminuir a potência do ruido

filtro = fir1(50, (1/Rb)*Nsamp);
sinal_NRZ_rx_filtrado = filter(filtro,1,sinal_NRZ_rx1);

figure(3)
plot(t, sinal_NRZ_rx_filtrado);
xlim([0 (1/Rb)*10]);
ylim([-10 10]);

%% Captura das amostras
sinal_NRZ_T1 = sinal_NRZ_rx1(Nsamp/2:Nsamp:end);
sinal_NRZ_T2 = sinal_NRZ_rx2(Nsamp/2:Nsamp:end);
sinal_NRZ_T3 = sinal_NRZ_rx3(Nsamp/2:Nsamp:end);
sinal_NRZ_T4 = sinal_NRZ_rx4(Nsamp/2:Nsamp:end);

sinal_RZ_T1 = sinal_RZ_rx1(Nsamp/2:Nsamp:end);
sinal_RZ_T2 = sinal_RZ_rx2(Nsamp/2:Nsamp:end);
sinal_RZ_T3 = sinal_RZ_rx3(Nsamp/2:Nsamp:end);
sinal_RZ_T4 = sinal_RZ_rx4(Nsamp/2:Nsamp:end);

%% Cálculo do limiar
info_hat_NRZ_1 = sinal_NRZ_T1 > limiar_NRZ;
info_hat_NRZ_2 = sinal_NRZ_T2 > limiar_NRZ;
info_hat_NRZ_3 = sinal_NRZ_T3 > limiar_NRZ;
info_hat_NRZ_4 = sinal_NRZ_T4 > limiar_NRZ;
info_hat_RZ_1 = sinal_RZ_T1 > limiar_RZ;
info_hat_RZ_2 = sinal_RZ_T2 > limiar_RZ;
info_hat_RZ_3 = sinal_RZ_T3 > limiar_RZ;
info_hat_RZ_4 = sinal_RZ_T4 > limiar_RZ;

%% Detecção de erro
num_erro_sinal_NRZ1 = sum(xor(info, info_hat_NRZ_1));
num_erro_sinal_NRZ2 = sum(xor(info, info_hat_NRZ_2));
num_erro_sinal_NRZ3 = sum(xor(info, info_hat_NRZ_3));
num_erro_sinal_NRZ4 = sum(xor(info, info_hat_NRZ_4));
num_erro_sinal_RZ1 = sum(xor(info, info_hat_RZ_1));
num_erro_sinal_RZ2 = sum(xor(info, info_hat_RZ_2));
num_erro_sinal_RZ3 = sum(xor(info, info_hat_RZ_3));
num_erro_sinal_RZ4 = sum(xor(info, info_hat_RZ_4));


%% Taxa erro de bit
taxa_erro_NRZ1 = num_erro_sinal_NRZ1/ length(info);
taxa_erro_NRZ2 = num_erro_sinal_NRZ2/ length(info);
taxa_erro_NRZ3 = num_erro_sinal_NRZ3/ length(info);
taxa_erro_NRZ4 = num_erro_sinal_NRZ4/ length(info);

taxa_erro_RZ1 = num_erro_sinal_RZ1/ length(info);
taxa_erro_RZ2 = num_erro_sinal_RZ2/ length(info);
taxa_erro_RZ3 = num_erro_sinal_RZ3/ length(info);
taxa_erro_RZ4 = num_erro_sinal_RZ4/ length(info);

