%% Aula 17 - Cosseno levantado
% 24/09

clear all;
clc;
close all;

sps = 80;     % Amostras por simbolo (fator de superamostragem)

Rb = 1e3;      % Taxa te bits [bit/s]
Tb = 1 / Rb;   % Tempo de bit [s]
Ta = Tb / sps; % Intervalo de amostragem [s]
fa = 1 /Ta;    % Frequencia de amostragem [amostrar/Hz]

bits = [1 1 1 0]; % Roubadinha
N_bits = length(bits);

x = 2 * bits - 1;
x_up = upsample(x,sps);

beta = 0.8; %fator de hollof (a mais que o pulso)
span = 31; % quantidade de voltas (numero impar)

h_TX = rcosdesign(beta,span,sps, 'normal');
h_TX = h_TX / max(h_TX);

stem(h_TX);
sinal_formatado = conv(h_TX, x_up); % ou convoluçao

N_samp = length(sinal_formatado);
t = (0 : N_samp-1)* Ta;

% Multiplica o comprimento do filtro pela duracao das amostras
t0 = Ta * (length(h_TX) - 1) / 2;
tf = t0 + (N_bits-1) * Tb;

t_bits = t0 : Tb : tf;

plot(t/1e-3, sinal_formatado);
hold on;
stem(t_bits/1e-3, x);
xlabel('t(ms)');