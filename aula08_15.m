close all;
clear all;
clc;

%% Parâmetros iniciais
fa = 100000;
t = 0:(1/fa):1;
f = (-fa/2):(fa/2);

m1_t = 1 * cos(2*pi*1000*t);
m2_t = 1 * cos(2*pi*2000*t);
m3_t = 1 * cos(2*pi*3000*t);

c1_t = 1 * cos(2*pi*9000*t);
c2_t = 1 * cos(2*pi*10000*t);
c3_t = 1 * cos(2*pi*11000*t);

figure(1)
subplot(311)
plot(t,m1_t)
xlim([0 100/fa]);
subplot(312)
plot(t,m2_t)
xlim([0 100/fa]);
subplot(313)
plot(t,m3_t)
xlim([0 100/fa]);

filtro_10k = fir1(500, [9000 11000]*2/fa);
filtro_12k = fir1(500, [11000 13000]*2/fa);
filtro_14k = fir1(500, [13000 15000]*2/fa);

s1_t = c1_t.*m1_t;
s2_t = c2_t.*m2_t;
s3_t = c3_t.*m3_t;


s1_t = filter(filtro_10k,1,s1_t);
s2_t = filter(filtro_12k,1,s2_t);
s3_t = filter(filtro_14k,1,s3_t);

m1_f = fftshift(fft(m1_t));
m2_f = fftshift(fft(m2_t));
m3_f = fftshift(fft(m3_t));

c1_f = fftshift(fft(c1_t));
c2_f = fftshift(fft(c2_t));
c3_f = fftshift(fft(c3_t));

s1_f =  fftshift(fft(s1_t));
s2_f =  fftshift(fft(s2_t));
s3_f =  fftshift(fft(s3_t));

figure (2)
subplot(311)
plot(f,abs(s1_f));
xlim([-11000 11000]);

subplot(312)
plot(f,abs(s2_f));
xlim([-13000 13000]);

subplot(313)
plot(f,abs(s3_f));
xlim([-15000 15000]);

sTotal_t = s1_t + s2_t + s3_t;

s1_t_filtrado = filter(filtro_10k,1,s1_t);
s2_t_filtrado = filter(filtro_12k,1,s2_t);
s3_t_filtrado = filter(filtro_14k,1,s3_t);

figure (3)
subplot(311)
plot(t,s1_t_filtrado);

subplot(312)
plot(t,s2_t_filtrado);

subplot(313)
plot(t,s3_t_filtrado);

figure (4)
plot(f,abs(fftshift(fft(sTotal_t))));

figure (5)
subplot(311);
plot(f, abs(fftshift(fft(s1_t_filtrado))));
xlim([-11000 11000]);

subplot(312);
plot(f, abs(fftshift(fft(s2_t_filtrado))));
xlim([-13000 13000]);

subplot(313);
plot(f, abs(fftshift(fft(s3_t_filtrado))));
xlim([-15000 15000]);
