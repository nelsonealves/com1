close all;
clear all;
clc;

%% Parâmetros iniciais
f1 = 1e3;%kHz
fa = f1*100;
t = 0:(1/fa):1;
f = -fa/2:fa/2;

m_t = 2*cos(2*pi*f1*t);     %%Sinal banda base em t
c_t = 2*cos(2*pi*(10*f1)*t); %%Portadora

m_f = fftshift(fft(m_t))/length(m_t);
c_f = fftshift(fft(c_t))/length(c_t);

figure(1)
subplot(221)
plot(t,m_t);
xlim([0 f1*2e-6]);
title('Sinal modulante no dominio do tempo');
subplot(222)
plot(t,c_t)
xlim([0 f1*2e-6]);
title('Sinal portadora dominio do tempo');
subplot(223)
plot(f,abs(m_f))
xlim([-f1-500 f1+500]);
title('Sinal modulante no dominio da frequência');
subplot(224)
plot(f,abs(c_f))
xlim([-10*f1-500 10*f1+500]);
title('Sinal portadora no dominio da frequência');

s_t = m_t.*c_t;
s_f = fftshift(fft(s_t))/length(s_t);

figure(2)
subplot(121)
plot(t,s_t);
xlim([0 f1*1e-6]);
title('Sinal modulado no dominio da tempo');
subplot(122)
plot(f,abs(s_f));
xlim([-11500 11500]);
title('Sinal modulado no dominio da frequência');

s_t_dem = s_t.*c_t;

filtro = fir1(100, 1000*2/fa);
s_t_dem1 = filter(filtro,1,s_t_dem);
s_f_dem = fftshift(fft(s_t_dem))/length(s_t_dem);

figure(3)
subplot(121)
plot(t,s_t_dem1);
xlim([0 0.01])
title('Sinal demodulado no dominio do tempo');
subplot(122)
plot(f,abs(s_f_dem));
xlim([-1500 1500]);
title('Sinal demodulado no dominio da frequência');



