clc; clear all;close all;

m1 = 0.25;
m2 = 0.5;
m3 = 0.75;
m4 = 1;
m5 = 1.5;

Ac = 1;

fm = 1000;
fc = 10000;
fa = 200*fc;

t = 0:1/fa:1;
f = -fa/2:fa/2;

mt = cos(2*pi*fm.*t);
ct = Ac*cos(2*pi*fc.*t);

s1_t = Ac*(1+m1*cos(2*pi*fm.*t)).*cos(2*pi*fc.*t);
s2_t = Ac*(1+m2*cos(2*pi*fm.*t)).*cos(2*pi*fc.*t);
s3_t = Ac*(1+m3*cos(2*pi*fm.*t)).*cos(2*pi*fc.*t);
s4_t = Ac*(1+m4*cos(2*pi*fm.*t)).*cos(2*pi*fc.*t);
s5_t = Ac*(1+m5*cos(2*pi*fm.*t)).*cos(2*pi*fc.*t);

s1_f = fftshift(fft(s1_t))/length(s1_t);
s2_f = fftshift(fft(s2_t))/length(s2_t);
s3_f = fftshift(fft(s3_t))/length(s3_t);
s4_f = fftshift(fft(s4_t))/length(s4_t);
s5_f = fftshift(fft(s5_t))/length(s5_t);

figure(1)
subplot(521)
plot(t,s1_t)
xlim([0 0.001])
title('Sinal modulado com u=0.25 no dominio do tempo');
subplot(523)
plot(t,s2_t)
xlim([0 0.001])
title('Sinal modulado com u=0.5 no dominio do tempo');
subplot(525)
plot(t,s3_t)
xlim([0 0.001])
title('Sinal modulado com u=0.75 no dominio do tempo');
subplot(527)
plot(t,s4_t)
xlim([0 0.001])
title('Sinal modulado com u=1 no dominio do tempo');
subplot(529)
plot(t,s5_t)
xlim([0 0.001])
title('Sinal modulado com u=1.5 no dominio do tempo');

subplot(522)
plot(f,abs(s1_f))
xlim([-11500 11500]);
title('Sinal modulado com u=0.25 no dominio da frequência');
subplot(524)
plot(f,abs(s2_f))
xlim([-11500 11500]);
title('Sinal modulado com u=0.5 no dominio da frequência');
subplot(526)
plot(f,abs(s3_f))
xlim([-11500 11500]);
title('Sinal modulado com u=0.75 no dominio da frequência');
subplot(528)
plot(f,abs(s4_f))
xlim([-11500 11500]);
title('Sinal modulado com u=1 no dominio do frequência');
subplot(5,2,10)
plot(f,abs(s5_f))
xlim([-11500 11500]);
title('Sinal modulado com u=1.5 no dominio do frequência');
%% Detector de envoltória

s1_t_abs = abs(s1_t);
s2_t_abs = abs(s2_t);
s3_t_abs = abs(s3_t);
s4_t_abs = abs(s4_t);
s5_t_abs = abs(s5_t);

figure(2)
subplot(511)
plot(t,s1_t_abs);
title('Sinal demodulado com u=0.25 no dominio do tempo');
xlim([0 0.005])
subplot(512)
plot(t,s2_t_abs);
xlim([0 0.005])
title('Sinal demodulado com u=0.5 no dominio do tempo');
subplot(513)
plot(t,s3_t_abs);
xlim([0 0.005])
title('Sinal demodulado com u=0.75 no dominio do tempo');
subplot(514)
plot(t,s4_t_abs);
xlim([0 0.005])
title('Sinal demodulado com u=1 no dominio do tempo');
subplot(515)
plot(t,s5_t_abs);
xlim([0 0.005])
title('Sinal demodulado com u=1.5 no dominio do tempo');

filtro = fir1(100, 1000*2/fa);

s1_t_filtrado = filter(filtro,1,s1_t_abs);
s2_t_filtrado = filter(filtro,1,s2_t_abs);
s3_t_filtrado = filter(filtro,1,s3_t_abs);
s4_t_filtrado = filter(filtro,1,s4_t_abs);
s5_t_filtrado = filter(filtro,1,s5_t_abs);

figure(3)
subplot(511)
plot(t,s1_t_filtrado);
xlim([0 0.005])
title('Detector de envoltória par u=0.25 no dominio do tempo');
subplot(512)
plot(t,s2_t_filtrado);
xlim([0 0.005])
title('Detector de envoltória par u=0.5 no dominio do tempo');
subplot(513)
plot(t,s3_t_filtrado);
xlim([0 0.005])
title('Detector de envoltória par u=0.75 no dominio do tempo');
subplot(514)
plot(t,s4_t_filtrado);
xlim([0 0.005])
title('Detector de envoltória par u=1 no dominio do tempo');
subplot(515)
plot(t,s5_t_filtrado);
xlim([0 0.005])
title('Detector de envoltória par u=1.5 no dominio do tempo');
