clear all;
close all;
clc;

A = 5;
Nsamp = 100;
Rb = 10e3;
var_ruido = 0.5;
info = [1 0 0 1 1 0 1 1 0 1];
t = 0:(1/(Rb*Nsamp)): (length(info)/Rb) - (1/(Rb*Nsamp));
info_up = upsample(info, Nsamp);
filtro_NRZ = ones(1, Nsamp);
info_NRZ = filter(filtro_NRZ, 1, info_up);
sinal_NRZ = info_NRZ*(2*A)-A;
sinal_RZ = info_NRZ*A;

figure(1)
subplot(413)
plot(t, sinal_NRZ);
ylim([-6 6]);

subplot(411)
plot(t, sinal_RZ);
ylim([-6 6]);

sinal_NRZ_rx = sinal_NRZ + sqrt(var_ruido)*randn(1, length(sinal_NRZ));
sinal_RZ_rx = sinal_RZ + sqrt(var_ruido)*randn(1, length(sinal_NRZ));

subplot(412)
plot(t, sinal_RZ_rx)
ylim([-6 6]);
subplot(414)
plot(t, sinal_NRZ_rx)
ylim([-6 6]);