clear all;
close all;
clc;

%% Cria as variaveis
fa = 10e3;
t = [0:1/fa:1-(1/fa)];
var1 = 1;
var2 = 5;
var3 = 15;
ruido1 = sqrt(var1)*randn(1,fa);
ruido2 = sqrt(var2)*randn(1,fa);
ruido3 = sqrt(var3)*randn(1,fa);

%% Gera o grafico da probabilidade do ruido acontecer
figure(1)
subplot(211)
hist(ruido1,100)
xlim([-10 10])
subplot(212)
hist(ruido2,100)
xlim([-10 10])

%% Cria dois sinais com ruido1 somado
s_rx1 = 5+ruido1;
s_rx0 = -5+ruido1;

figure(2)
hist(s_rx1,100)
xlim([-15 15])
hold on
hist(s_rx0,100)
xlim([-15 15])

%% Cria dois sinais com ruido2 somados
s_rx0 = -5+ruido2;
s_rx1 = 5+ruido2;

figure(3)
hist(s_rx1,100)
xlim([-15 15])
hold on
hist(s_rx0,100)
xlim([-15 15])

%% Cria dois sinais com ruido3 somados
s_rx1 = 5+ruido3;
s_rx0 = -5+ruido3;

figure(4)
hist(s_rx1,100)
xlim([-15 15])
hold on
hist(s_rx0,100)
xlim([-15 15])


%% Mostra a amplitude dos ruidos
figure(5)
subplot(311)
plot(t,ruido1)
subplot(312)
plot(t,ruido2)
subplot(313)
plot(t,ruido3)

%% Mostra a correlação do ruido
figure(6)
f_corr = xcorr(ruido1);
plot(f_corr)

%% Cria um filtro com ordem 50
filtro1 = fir1(50,1000*2/fa);
filtro2 = fir1(50,3000*2/fa);
ruido1_filtrado1 = filter(filtro1,1,ruido1);
ruido1_filtrado2 = filter(filtro2,1,ruido1);

figure(7)
plot(t,ruido1,'b')
hold on
plot(t,ruido1_filtrado2,'y')
plot(t,ruido1_filtrado1,'r')


%% Mostra o sinal do ruido, e o sinal do ruido filtrado 
%% em duas taxas de amostragens diferentes
figure(8)
subplot(311)
hist(ruido1,100)
xlim([-5 5])
subplot(312)
hist(ruido1_filtrado2,100)
xlim([-5 5])
subplot(313)
hist(ruido1_filtrado1,100)
xlim([-5 5])
