
fa = 10e3;
k = 3;
Rb = k*fa;
l = 2^k;
t = 0:1/fa:1;
A = 30;
passo = (A*2)/l;
m = (A + A*cos(2*pi*1000*t))./passo;

m_round = round(m);
m_round(m_round == 8) = 7;

m_rect = rectpulse(m_round,10);
figure(1)
plot(m_rect)
xlim([0 300])
figure(2)
stem(t,m_round)
xlim([0 0.005])
m_bin = de2bi(m_round);

%% Tem que fazer o reshape depois.






