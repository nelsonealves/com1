clear all;
close all;
clc;

%% Parametros iniciais
fa = 10e3;
t = 0:1/fa:1-(1/fa);
f = -fa/2:fa/2-1;

%% 1. Gerar um vetor representando um ruído com distribuição normal utilizando a função 'randn' do matlab. 
%  Gere 1 segundo de ruído considerando um tempo de amostragem de 1/10k. 
var1 = 1;
ruido_t = randn(1,fa);

%% 2. Plotar o histograma do ruído para observar a distribuição
%  Gaussiana. Utilizar a função 'histogram'
figure(1)
hist(ruido_t,100)
title('Histograma do vetor aleatório');
xlabel('Dados');
ylabel('Densidade');

%% 3. Plotar o ruído no domínio do tempo e da frequência
figure(2)
subplot(212)
ruido_f = fftshift(fft(ruido_t))/length(f);
plot(f,ruido_f);
title('Ruido no dominio da frequência');
xlabel('Frequência [Hz]')
ylabel('Volts [V]')
subplot(211)
plot(t,ruido_t);
title('Ruido no dominio do tempo');
xlabel('Tempo [t]')
ylabel('Volts [V]')

%% 4. Utilizando a função 'xcorr', plote a função de autocorrelação do ruído.
figure(3)
f_corr = xcorr(ruido_t);
plot(f_corr)
title('Função autocorrelação');
xlabel('Frequência [Hz]')

%% 5. Utilizando a função 'filtro=fir1(50,(1000*2)/fs)', realize uma
%  operação de filtragem passa baixa do ruído. Para visualizar a
%  resposta em frequência do filtro projetado, utilize a função
% 'freqz'. 
filtro = fir1(50,(1000*2)/fa);
figure(4)
freqz(filtro);

%% 6. Plote, no domínio do tempo e da frequência, a saída do filtro
%  e o histograma do sinal filtrado 
sinal_filtrado_t = filter(filtro,1,ruido_t);

figure(5)
subplot(211)
plot(t,sinal_filtrado_t);
title('Saída no dominio do tempo');
xlabel('Tempo [t]')
ylabel('Volts [V]')

subplot(212)
plot(f,abs(fftshift(fft(sinal_filtrado_t)))/length(f));
title('Saída no dominio da frequência');
xlabel('Frequência [Hz]')
ylabel('Volts [V]')
figure(7)
hist(sinal_filtrado_t,100);
title('Histograma do vetor aleatório');
xlabel('Dados');
ylabel('Densidade');
