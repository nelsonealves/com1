close all;
clear all;
clc;

%% Parametros iniciais
f1 = 1000;
f2 = 3000;
f3 = 5000;

fa = 20*f3;

t = 0:1/fa:1-1/fa;
f = -fa/2:(fa/2)-1;

%% 1. Gerar um sinal s(t) composto pela somatória de 3 senos com amplitudes
%  de 5V, 5/3V e 1V e frequências de 1, 3 e 5 kHz, respectivamente.
sin1_t = 5*cos(2*pi*f1*t);
sin2_t = (5/3)*cos(2*pi*f2*t);
sin3_t = 1*cos(2*pi*f3*t);

y_t = sin1_t + sin2_t + sin3_t;
y_f = fftshift(fft(y_t))/length(f);

%% 2. Plotar em uma figura os três cossenos e o sinal 's ' no domínio do tempo e
%  da frequência
figure(1)
subplot(211)
plot(t,y_t)
xlim([0 0.004])
title('Sinal s(t)');
xlabel('Tempo [t]')
ylabel('Volts [V]')
subplot(212)
plot(f,abs(y_f));
xlim([-6000 6000]);
title('Sinal s(f)');
xlabel('Frequência [Hz]')
ylabel('Volts [V]')

%% 3. Gerar 3 filtros ideais:
%  • Passa baixa (frequência de corte em 2kHz)
%  • Passa alta (banda de passagem acima de 4kHz)
%  • Passa faixa (banda de passagem entre 2 e 4kHz)
filtro_pb_2k = [zeros(1,48000) ones(1,4000) zeros(1,48000)];
filtro_pa_4k = [ones(1,46000) zeros(1,8000) ones(1,46000)];
filtro_pf_2k_4k = [zeros(1,46000) ones(1,2000) zeros(1,4000) ones(1,2000) zeros(1,46000)];

%% 4. Plotar em uma figura a resposta em frequência dos 3 filtros
figure(3)
subplot(311);
plot(f,filtro_pb_2k)
title('Filtro passa baixa com fc=2kHz');
xlabel('Tempo [t]')
ylabel('Volts [V]')
ylim([-0.5 1.5])
xlim([-2500 2500])

subplot(312);
plot(f,filtro_pa_4k)
title('Filtro passa alta com fc=4kHz');
xlabel('Tempo [t]')
ylabel('Volts [V]')
ylim([-0.5 1.5])
xlim([0 4500])

subplot(313);
plot(f,filtro_pf_2k_4k)
title('Filtro passa faixa com fc entre 2kHz e 4kHz');
xlabel('Tempo [t]')
ylabel('Volts [V]')
ylim([-0.5 1.5])
xlim([-5000 5000])

%% 5. Passar o sinal s(t) através dos 3 filtros e plotar as saídas, no domínio do
%  tempo e da frequência, para os 3 casos
sen1_f = filtro_pb_2k.*y_f;
sen2_f = filtro_pa_4k.*y_f;
sen3_f = filtro_pf_2k_4k.*y_f;

% Dominio da frequencia


% Transformada inversa
sen1_t = ifft(ifftshift(sen1_f))*length(f);
sen2_t = ifft(ifftshift(sen2_f))*length(f);
sen3_t = ifft(ifftshift(sen3_f))*length(f);

figure(4)
subplot(321)
plot(f,abs(sen1_f));
title('Resposta da saida em frequência do filtro baixa');
xlabel('Frequência [Hz]')
ylabel('Volts [V]')
ylim([0 3])

subplot(323)
plot(f,abs(sen2_f));
title('Resposta da saida em frequência do filtro alta');
xlabel('Frequência [Hz]')
ylabel('Volts [V]')
ylim([0 1])

subplot(325)
plot(f,abs(sen3_f));
title('Resposta da saida em frequência do filtro passa faixa');
xlabel('Frequência [Hz]')
ylabel('Volts [V]')
% Dominio do tempo

subplot(322)
plot(t, sen1_t);
xlim([0 0.001])
title('Resposta do filtro passa baixa no dominio do tempo');
xlabel('Tempo [t]')
ylabel('Volts [V]')


subplot(324)
plot(t, sen2_t);
xlim([0 0.001])
title('Resposta do filtro passa alta no dominio do tempo');
xlabel('Tempo [t]')
ylabel('Volts [V]')


subplot(326)
plot(t, sen3_t);
xlim([0 0.001])
title('Resposta do filtro passa faixa no dominio do tempo');
xlabel('Tempo [t]')
ylabel('Volts [V]')