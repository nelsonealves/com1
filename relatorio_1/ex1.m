close all;
clear all;
clc;

%% Parametros iniciais 
f1 = 1000;
f2 = 3000;
f3 = 5000;

fa = 60*f3;

t = 0:1/fa:1;
f = -fa/2:fa/2;

%% 1. Gerar um sinal s(t) composto pela somatória de 3 senos com
%  amplitudes de 6V, 2V e 4V e frequências de 1, 3 e 5 kHz,
%  respectivamente.
cos1_t = 6*cos(2*pi*f1*t);
cos2_t = 2*cos(2*pi*f2*t);
cos3_t = 4*cos(2*pi*f3*t);

cos1_f = fftshift(fft(cos1_t))/length(f);
cos2_f = fftshift(fft(cos2_t))/length(f);
cos3_f = fftshift(fft(cos3_t))/length(f);

figure(1)
subplot(321)
plot(t,cos1_t);
title('Sinal s(t): 6*sin(2*pi*1000t)');
xlabel('Tempo [t]')
ylabel('Volts [V]')
xlim([0 0.001]);

subplot(323)
plot(t,cos2_t);
title('Sinal s(t): 2*sin(2*pi*3000t)');
xlim([0 0.001]);
xlabel('Tempo [t]')
ylabel('Volts [V]')

subplot(325)
plot(t,cos3_t);
title('Sinal s(t): 4*sin(2*pi*5000t)');
xlim([0 0.001]);
xlabel('Tempo [t]')
ylabel('Volts [V]')

subplot(322)
plot(f,abs(cos1_f));
title('Sinal s(f): 4*sin(2*pi*5000t)');
xlim([-2000 2000]);
xlabel('Frequência [Hz]')
ylabel('Volts [V]')

subplot(324)
plot(f,abs(cos2_f));
xlim([-4000 4000]);
title('Sinal s(f): 4*sin(2*pi*5000t)');
xlabel('Frequência [Hz]')
ylabel('Volts [V]')

subplot(326)
plot(f,abs(cos3_f));
title('Sinal s(f): 4*sin(2*pi*5000t)');
xlim([-6000 6000]);
xlabel('Frequência [Hz]')
ylabel('Volts [V]')

%% 2. Plotar em uma figura os três cossenos e o sinal 's ' no domínio do
%  tempo e da frequência.
y_t = cos1_t + cos2_t + cos3_t;
y_f = fftshift(fft(y_t))/length(f);

figure(2)
subplot(211)
plot(t,y_t);
xlim([0 0.004]);
subplot(212)
plot(f,abs(y_f));
xlim([-6000 6000]);

%% 3. Utilizando a função 'norm', determine a potência média do sinal 's'.

mag1 = ((norm(cos1_t)).^2)/length(t);
pot_cos1 = (1/length(t))*sum(cos1_t.^2);

mag2 = ((norm(cos2_t)).^2)/length(t);
pot_cos2 = (1/length(t))*sum(cos2_t.^2);

mag3 = ((norm(cos3_t)).^2)/length(t);
pot_cos3 = (1/length(t))*sum(cos3_t.^2);

mag4 = ((norm(y_t)).^2)/length(t);
pot_y_t = (1/length(t))*sum(y_t.^2);

%% 4. Utilizando a função 'pwelch', plote a Densidade Espectral de
%  Potência do sinal 's'.

figure(3)

pwelch(y_t);